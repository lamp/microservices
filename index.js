var express = require("express");
var morgan = require("morgan");
var serveIndex = require("serve-index");

var app = express();
app.set("trust proxy", "loopback");
app.listen(8080, "::1");

app.use(morgan(`:date[iso] :remote-addr :method :url ":req[user-agent]" :referrer`));
app.all("*.js", function(req, res, next) {
	require(`./root${req.path}`)(req, res, next)?.catch?.(next);
});
app.use(express.static("root"), serveIndex("root", {icons: true}));
