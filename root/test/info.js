module.exports = function(req, res) {
	res.type('text').send(require('util').inspect({
		ip: req.ip,
		method: req.method,
		url: req.url,
		query: req.query,
		headers: req.headers,
		date: new Date()
	}));
};