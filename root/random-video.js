var fs = require("fs");
module.exports = function(req, res) {
	var v = fs.readdirSync(__dirname + "/videos/");
	var b = v[Math.floor(Math.random()*v.length)];
	res.redirect(`videos/${b}`);
}
