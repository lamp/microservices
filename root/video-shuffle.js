var getVideoLength = require("video-length");
var fs = require("fs");

var currentVideo;

module.exports = function (req, res) {
	if (currentVideo) {
		res.redirect(`videos/${currentVideo}`);
	} else {
		console.debug("v new");
		var videos = fs.readdirSync(__dirname+"/videos/");
		currentVideo = videos[Math.floor(Math.random() * videos.length)];
		res.redirect(`videos/${currentVideo}`);
		getVideoLength(__dirname+"/videos/"+currentVideo, {bin: "/usr/bin/mediainfo"}).then(length => {
			console.debug("v len", length);
			setTimeout(() => {
				currentVideo = undefined;
				console.debug("v reset");
			}, (length - 5) * 1000);
		}).catch(error => console.error(error.message));
	}
}